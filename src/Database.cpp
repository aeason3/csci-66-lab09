/*
 * Database.cpp
 *
 *  Created on: Oct 26, 2018
 *      Author: Aaron James Eason
 */

#include <algorithm>

#include "Database.h"
#include "Record.h"
#include "DatabaseFilter.h"

/*
 * Destructor
 */
Database::~Database() {
}

/*
 * Adds a record to this database
 */
void Database::addRecord(Record& record) {
	this->records.push_back(record);
}

/*
 * Adds a filter to the database, for usage when printing data
 */
void Database::addFilter(DatabaseFilter* filter) {
	this->filters.push_back(filter);
}

/*
 * Sort the Records in place according to the "natural ordering" specified in our overloaded (<) operator
 */
void Database::sort() {
	std::sort(this->records.begin(), this->records.end());
}

/*
 * Performs the provided operation on each Record in the Database
 */
void Database::forEachRecord(void (*lambda)(Record& record, const int& row)) {
	for(unsigned int row = 0; row < this->records.size(); row++) {
		lambda(this->records[row], row);
	}
}

/*
 * Returns true if the database already contains the provided Record
 */
bool Database::contains(const Record& record) const {
	for (Record entry : this->records) {
		if (entry == record)
			return true;
	}
	return false;
}

/*
 * Utility for calculating the largest (longest string) field in all of our Records
 */
const Record& Database::getLargestRecordByColumn(int column) const {
	return *std::max_element(this->records.begin(), this->records.end(),
			[&column](const Record& lhs, const Record& rhs) {
				int lsize = (int) lhs.width() > column ? lhs[column].size() : 0;
				int rsize = (int) rhs.width() > column ? rhs[column].size() : 0;
				return lsize < rsize;
			});
}

/*
 * Overloaded << operator that outputs the entire database based on currently configured filters
 */
std::ostream& operator <<(std::ostream& stream, const Database& rhs) {

	// stream each row one at a time
	for (unsigned int row = 0; row < rhs.records.size(); row++) {

		const Record& record = rhs.records[row];

		// stream each field one column at a time
		for (unsigned int column = 0; column < record.width(); column++) {

			std::string field = record[column];

			// process filters in most-recent-first order
			for (int index = rhs.filters.size() - 1; index >= 0; index--) {
				if ((*rhs.filters[index]).filter(&field, row, column))
					break; // if a filter reports this field shoudln't be processed further, we break out
			}

			// stream the field
			stream << field;
		}
		stream << std::endl;
	}

	return stream;
}
