/*
 * Database.h
 *
 *  Created on: Oct 26, 2018
 *      Author: Aaron James Eason
 */

#ifndef DATABASE_H_
#define DATABASE_H_

#include <iostream>
#include <vector>
#include <string>

#include "Record.h"

class DatabaseFilter;
class Database {
public:
	/*
	 * Destructor
	 */
	virtual ~Database();

	/*
	 * Adds a record to this database
	 */
	void addRecord(Record& record);

	/*
	 * Adds a filter to the database, for usage when printing data
	 */
	void addFilter(DatabaseFilter*);

	/*
	 * Sort the Records in place according to the "natural ordering" specified in our overloaded (<) operator
	 */
	void sort();

	/*
	 * Performs the provided operation on each Record in the Database
	 */
	void forEachRecord(void(*lambda)(Record& record, const int& row));

	/*
	 * Returns true if the database already contains the provided Record
	 */
	bool contains(const Record& record) const;

	/*
	 * Utility for calculating the largest (longest string) field in all of our Records
	 */
	const Record& getLargestRecordByColumn(int column) const;

	/*
	 * Overloaded << operator that outputs the entire database in CSV format
	 */
	friend std::ostream& operator <<(std::ostream& stream, const Database& rhs);

private:
	std::vector<Record> records;
	std::vector<DatabaseFilter*> filters;
};

#endif /* DATABASE_H_ */
