/*
 * DatabaseFilter.h
 *
 *  Created on: Oct 26, 2018
 *      Author: Aaron James Eason
 */

#ifndef DATABASEFILTER_H_
#define DATABASEFILTER_H_

#include <set>
#include <string>
#include <iomanip> // I/O Manipulation
#include <algorithm>

#include "Database.h"

/*
 * A Generic type that can be used to filter Database stream output
 */
struct DatabaseFilter {

	virtual ~DatabaseFilter() {
	}
	virtual bool filter(std::string*, const int&, const int&) const = 0;
};

/*
 * A DatabaseFilter that causes stream output to be in CSV format, complete with self-indexing rows
 */
struct CSVPrinter: public DatabaseFilter {

	~CSVPrinter() {
	}
	bool filter(std::string* field, const int& row, const int& column) const {

		std::stringstream formatter;

		// add new index column
		if (column == 0) {
			formatter << (row + 1);
		}

		// add the "C" part of "CSV"
		formatter << "," << *field;

		// actually commit our changes
		*field = formatter.str();
		return true;
	}
};

/*
 * A DatabaseFilter that causes stream output to be in a tabular format, with evenly spaced columns, etc.
 */
struct TablePrinter: public DatabaseFilter {
	TablePrinter(Database* db,
			const std::initializer_list<std::string>& schema,
			const std::initializer_list<std::ios_base&(*)(std::ios_base&)> alignments) {

		this->schema = schema;
		this->alignments = alignments;

		// calculate how large each column will need to be to accommodate its contents
		for (unsigned int column = 0; column < schema.size(); column++) {
			const Record& largest = db->getLargestRecordByColumn(column);
			columnWidths.push_back(std::max(
					largest.width() > column ? largest[column].size() : 0,
					this->schema[column].size()));
		}
	}
	~TablePrinter() {
	}

	bool filter(std::string* field, const int& row, const int& column) const {

		std::stringstream formatter;

		// if we're just starting to print, lets print the table header first
		if (column == 0 && row == 0) {
			for (unsigned int i = 0; i < this->schema.size(); i++) {

				// grab the title string for this column from our schema
				const std::string& title = this->schema.begin()[i];

				// print the column headers TODO: center formatter :)
				formatter << std::left << std::setw(columnWidths[i]) << title << "  ";
			}
			formatter << std::endl;

			// next, the header separator
			for(unsigned int i = 0; i < this->schema.size(); i++)
				formatter << std::string(columnWidths[i]+2, '-');
			formatter << std::endl;
		}

		// now let's worry about printing this particular field
		formatter << this->alignments[column] << std::setw(columnWidths[column]) << *field << "  ";

		// actually commit our changes
		*field = formatter.str();
		return true;
	}

private:
	std::vector<std::string> schema;
	std::vector<int> columnWidths;
	std::vector<std::ios_base&(*)(std::ios_base&)> alignments;
};

/*
 * A DatabaseFilter that causes given columns and rows to be rounded to a given precision
 */
struct PrecisionFilter: public DatabaseFilter {
	PrecisionFilter(const int& precision, std::initializer_list<int> rows, std::initializer_list<int> columns) {
		this->rows = rows;
		this->columns = columns;
		this->precision = precision;
	}
	~PrecisionFilter() {
	}

	bool filter(std::string* field, const int& row, const int& column) const {

		// if we aren't configured to act on this field, abort
		if(this->columns.find(column) == columns.end() && this->rows.find(row) == rows.end())
			return false;

		std::stringstream formatter;

		// parse our field to a double (or int), then set our field to the required precision
		double value = std::stod(*field);
		formatter << std::setprecision(this->precision) << std::fixed << value;

		// actually commit our changes
		*field = formatter.str();
		return false;
	}

private:
	std::set<int> rows;
	std::set<int> columns;
	int precision;
};

#endif /* DATABASEFILTER_H_ */
