//============================================================================
// Name        : Lab9.cpp
// Author      : Aaron James Eason
// Version     : Perpetual Beta
// Copyright   : MIT
// Description : "Dedupe" a CSV file, AND sort it!
//============================================================================

#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>

#include "Database.h"
#include "Record.h"
#include "DatabaseFilter.h"

using namespace std;

int main() {
	// first, let's query for the path to the CSV file
	cout << "Please enter the absolute path to the CSV file: ";
	string path;
	getline(cin, path);

	// split the provided path into a directory and filename
	string directory, filename;
	int lastSlash = path.rfind('/');
	directory = path.substr(0, lastSlash + 1);
	filename = path.substr(lastSlash + 1);

	// verify we split the path correctly
	cout << "The directory is: " << directory << endl;
	cout << "The filename is: " << filename << endl;

	// attempt to open the requested database file
	ifstream database_file(path);
	if (!database_file.is_open()) {
		cout << "failed to open the database file requested" << endl;
		return -1;
	}

	// create a Database of records to track
	Database database = Database();

	// for each line in the database
	for (string line; getline(database_file, line);) {

		// we shave off the first column because we don't care about the "index"
		istringstream row;
		row.str(line);
		row.ignore(256, ',');

		// create a new record from the remaining row
		Record record(row);

		// if this record is a duplicate, move on to the next row
		if(database.contains(record)) {
			continue;
		}

		// otherwise, add the record to the database
		database.addRecord(record);
	}

	// close the old file
	database_file.close();

	// LAB 09 WORK

	database.sort();

	// END LAB 09 WORK :)

	// create the new database file
	string newPath = directory + "new_" + filename;
	cout << "writing new database file: " << newPath << endl;

	// check for file creation issues
	ofstream new_database_file(newPath);
	if(!new_database_file.is_open()) {
		cout << "failed to open the new database file for writing" << endl;
		return -2;
	}

	// LAB 09 CHANGES

	// adds a filter that causes the database to be printed in CSV format
	CSVPrinter csvPrinter = CSVPrinter();
	database.addFilter(&csvPrinter);

	// END LAB 09 CHANGES

	// outputs the entire database in the current format
	new_database_file << database;

	new_database_file.close();
	cout << "new (depuped and sorted) database file written" << endl;

	// LAB 09 WORK

	// lets calculate that last column
	database.forEachRecord([](Record& record, const int& row){

		stringstream field;

		// parse the quantity and cost in each record
		double quantity = stod(record[2]);
		double cost = stod(record[3]);

		field << (quantity * cost);

		// append the total column
		record += field.str();
	});

	// output report
	cout << "Here's a report of the exported data:" << endl;

	// formatters to adhere to the requirements of Lab09
	TablePrinter tablePrinter = TablePrinter(&database,
			{"Department", "Item Code", "Quantity", "Cost", "Total"}, // headers
			{std::left, std::left, std::left, std::right, std::right}); // alignments
	database.addFilter(&tablePrinter);

	PrecisionFilter precisionFilter = PrecisionFilter(
			2, // precision
			{}, // rows to filter
			{3, 4}); // columns to filter
	database.addFilter(&precisionFilter);


	// finally, stream out our report to stdout
	cout << database;

	// END LAB 09 WORK

	return 0;
}
