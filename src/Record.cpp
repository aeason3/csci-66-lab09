/*
 * Record.cpp
 *
 *  Created on: Oct 21, 2018
 *      Author: Aaron James Eason
 */

#include "Record.h"

/**
 * Constructor and destructor
 */
Record::Record(std::istringstream& line) {
	// break comma-delimited input into string fields
	for (std::string field; std::getline(line, field, ',');) {
		this->fields.push_back(field);
	}
}
Record::~Record() {
}

/*
 * Returns the number of columns in this Record
 */
unsigned long int Record::width() const {
	return this->fields.size();
}

/*
 * Addition Assignment operator (adds a new field)
 */
Record& Record::operator +=(const std::string field) {
	this->fields.push_back(field);
	return *this;
}

/**
 * Comparison operator
 */
bool operator ==(const Record& lhs, const Record& rhs) {
	return lhs.fields == rhs.fields;
}

/**
 *  < (less than) operator
 */
bool operator <(const Record& lhs, const Record& rhs) {
	return lhs.fields[0] == rhs.fields[0] ? // if the first columns are the same
	lhs.fields[1] < rhs.fields[1] : // compare the second columns
	lhs.fields[0] < rhs.fields[0]; // otherwise, sort based on the first columns
}

/*
 * Returns the field at the specified index
 */
const std::string& Record::operator [](int index) const {
	return this->fields[index];
}
