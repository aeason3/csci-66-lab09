/*
 * Record.h
 *
 *  Created on: Oct 21, 2018
 *      Author: Aaron James Eason
 */

#ifndef RECORD_H_
#define RECORD_H_

#include <sstream>
#include <vector>

class Record {
public:
	/*
	 * Constructor and destructor
	 */
	Record(std::istringstream& line);
	virtual ~Record();

	/*
	 * Returns the number of columns in this Record
	 */
	unsigned long int width() const;

	/*
	 * Addition Assignment operator (adds a new field)
	 */
	Record& operator +=(const std::string field);

	/*
	 * Comparison operator
	 */
	friend bool operator ==(const Record& lhs, const Record& rhs);

	/*
	 *  < (less than) operator
	 */
	friend bool operator <(const Record& lhs, const Record& rhs);

	/*
	 * Returns the field at the specified index
	 */
	const std::string& operator [](int index) const;

private:
	std::vector<std::string> fields;
};

#endif /* RECORD_H_ */
